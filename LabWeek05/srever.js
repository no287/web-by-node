var express = require('express');
var app = express();

app.use(express.static('public'));
app.get('/index.html',function(req,res){
    res.sendFile(_dirname+"/public"+"index.html");
})

app.get('/process_get',function(req,res){
    response={
        first_name:req.query.first_name,
        last_name:req.query.last_name,
        date_of_birth:req.query.date_of_birth,
        address:req.query.address
    };
    console.log(response);
    res.end(JSON.stringify(response));
})

app.get('/', function (req, res) {
    res.send('Hello World');
});

app.listen(8081,function(){
    console.log("Server is started already !!")
})
