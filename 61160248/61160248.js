var express = require('express')
var app = express()

app.get('/', function (req, res) {
  res.send('Welcome to CS page')
})

app.use(express.static('public'))
app.get('/about', function (req, res) {
    res.sendFile(__dirname+"/public/"+"student.html");
})

app.get('/img', function (req, res) {
  res.sendFile(__dirname+"/public"+"/img/"+"7-Eleven-logo.png")
})

app.get('/login',function(req,res){
    res.sendFile(__dirname+"/public/"+"login.html");
})

app.get('/process_get', function (req, res) {
  response = {
    user: req.query.user,
    password: req.query.password
  }
  res.end(JSON.stringify(response))
})

app.get('/syn', function (req, res) {
  res.send(' ')
  var fs = require('fs')
  var data = fs.readFileSync('information.txt')

  console.log(data.toString())
  console.log('Do other things')
})

app.get('/asyn', function (req, res) {
  res.send(' ')
  var fs = require('fs')
  var data = fs.readFile('information.txt', function (err, data) {
    console.log(data.toString())
  })
  console.log('Do other things')
})

app.listen(3000, function () {

})     
