const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");

let users = require("./user.json");
let app = express();

app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/user/all/", function (req, res, next) {
  return res.status(200).json({
    code: 1,
    message: "OK",
    data: users,
  });
});

app.get("/user/show/:id", (req, res) => {
  const itemId = req.params.id;
  const item = users.find((_item) => _item.id.toString() === itemId);
  if (item) {
    res.json(item);
  } else {
    res.json({ message: `item ${itemId} doesn't exist` });
  }
  //return res.status(200).json({
  //code: 1,
  //message: "OK",
  //data: users[itemId],
  //});
});

app.delete("/user/del/:id", function(req, res, next) {
  	    const removeId = req.params.id; // รับค่า params จาก url 
  	    const position = users.findIndex((val) => { // หา Index จาก array users
	        return val.id == removeId;
  	    });
  	    users.splice(position, 1); // ลบสมาชิกใน array
  	    return res.status(200).json({
  	        code: 1,
  	        message: 'OK',
  	        data: users
  	    })
  	});

app.use(express.static("public"));
app.get("/post", function (req, res) {
  res.sendFile(__dirname + "/public/" + "post.html");
});

app.post("/user/add/", function (req, res, next) {
  //app.use(express.static('public'));
  //res.sendFile(_dirname+"/public/"+"post.html");
  let user = {}; // สร้าง Object user
  user.id = users.length + 1; // id จำลองมาจาก auto increment ใน database โดยนับจากจำนวน length เริ่มต้นที่ 1
  user.name = req.body.name; // รับค่าจาก body ที่ส่งมาทาง client จาก tag ที่ชื่อว่า "name"
  user.age = Number(req.body.age); // รับค่าจาก body ที่ส่งมาทาง client จาก tag ที่ชื่อว่า "age" พร้อมกับแปลงค่านั้นเป็นตัวเลขโดยฟังก์ชั่น Number()
  user.movie = req.body.movie; // รับค่าจาก body ที่ส่งมาทาง client จาก tag ที่ชื่อว่า "movie"
  users.push(user); // ทำการเพิ่ม Object user เข้าไปใน Array users
  console.log("Users :", user.name, "Created!");
  return res.status(201).json({
    code: 1,
    message: "OK",
    data: users,
  });
});

app.get("/put", function (req, res) {
  res.sendFile(__dirname + "/public/" + "put.html");
});

app.post("/user/edit/", function (req, res, next) {
  //app.use(express.static('public'));
  const replaceId = req.body.id; // รับค่า params จาก url
  const position = users.findIndex(function (val) {
    // หา Index จาก array users
    return val.id == replaceId;
  });
  console.log(users[position]);
  //res.sendFile(_dirname+"/public/"+"put.html");
  users[position].name = req.body.name; // ทำการกำหนดค่า name ใหม่เข้าไปจาก req.body ที่รับเข้ามา
  users[position].age = Number(req.body.age); // ทำการกำหนดค่า age ใหม่เข้าไปจาก req.body ที่รับเข้ามา
  users[position].movie = req.body.movie; // ทำการกำหนดค่า movie ใหม่เข้าไปจาก req.body ที่รับเข้ามา
  return res.status(200).json({
    code: 1,
    message: "OK",
    data: users,
  });
});

app.listen(3000, function () {
  console.log("Server Listen at http://localhost:3000");
  //console.log("Users :", users);
});
