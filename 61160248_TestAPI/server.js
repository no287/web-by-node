const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
//const path = require('path')

let dataUser = require('./user.json')
let app = express()

app.use(bodyParser.json()) //การเรียกใช้งาน parse json
app.use(morgan('dev')) //การเรียกใช้งาน morgam
app.use(cors()) //การเรียกใช้ cors
app.use(bodyParser.urlencoded({ extended: true })) //แปลงข้อมูลที่ส่งมาจากฟอร์ม

app.set('view engine', 'ejs')

//app.use(express.static('public')) //เรียกใช้โฟลเดอร์

app.get('/', function (req, res) {
  res.send('Hello World') //แสดง Hello World เมื่อค่าจาก url = /
})

app.get('/login', function (req, res) {
  //แสดง Form login เมื่อค่าจาก url = /login
  res.sendFile(__dirname + '/public/' + 'Login.html') //นำข้อมูลไฟล์จากโฟลเดอร์ public ที่ชื่อ Login.html มาแสดง
})

app.post('/user/member', function (req, res, next) {
  //ทำเมื่อกดปุ่ม summit จาก Form login
  const email = req.body.email //รับค่าที่ส่งมาจาก body จาก tag ที่ชื่อ email
  const password = req.body.password //รับค่าที่ส่งมาจาก body จาก tag ที่ชื่อ password
  const position = dataUser.find(position => position.email === email) //การหาข้อมูลจากไฟล์ user.json
  const position2 = dataUser.find(position2 => position2.password === password)

  if (position == null || position2 == null) {
      res.render("error")
      res.status(500)
  }else if (position.memder == 'yes') {
    //res.json(position);
    //res.sendFile(__dirname + '/public/' + 'LoginSuccess.html')
    res.render('showMember', {
      detail: 'Name : ' + position.name,
      detail2: 'Age : ' + position.age,
      detail3: 'Movie : ' + position.movie,
      detail4: 'Email : ' + position.email
    })
    res.status(200)
  } else {
    //res.sendFile(__dirname + '/public/' + 'LoginFail.html')
    //res.json({ message: `no memder`+position });
    res.render('noMember')
    res.status(404)
  }

  //if (position) {
  //res.json(position);
  //} else {
  //res.json({ message: `item ${position} doesn't exist` });
  //}
  //return res.status(200).json({
  //code: 1,
  //message: "OK",
  //data: data,
  //});
})

app.listen(3000, function () {
  console.log('Server Listen at http://localhost:3000')
  //console.log("Users :", data);
})
