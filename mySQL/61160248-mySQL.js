const express = require('express')
const app = express()
const bodyParser = require('body-parser')
app.set('view engine', 'pug')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const mysql = require('mysql')
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'sitepoint'
})

con.connect(err => {
  //เช็คว่ามัน connect มั้ย
  if (err) {
    console.log('Error connecting to Db')
    console.log(err)
    return
  }
  console.log('Connection established')
})

*app.get('/',(req,res) => {
	con.query('SELECT * FROM authors',(err,result) => {
		res.send('Connection established');
	})
});

app.get('/home', (req, res) => {
  con.query('SELECT * FROM authors', (err, result) => {
    res.render('showCon', {
      authors: result
    })
  })
})

app.get('/add', (req, res) => {
  res.render('add')
})

app.post('/add', (req, res) => {
  const name = req.body.name
  const city = req.body.city
  const author = {
    name: name,
    city: city
  }
  con.query('INSERT INTO authors SET ?', author, (err, res) => {
    // เป็นคำสั่งเพิ่ม มีการระบุชื่อตาราง
    if (err) throw err // หากพบ err  ให้แสดง err
    console.log('Last insert ID:', res.insertId) // แสดงผลการเพิ่มเป็นไอดีตัวที่เพิ่มล่าสุด
  })
  return res.redirect('/') //กลับไปที่หน้าแรก
})

app.get('/edit/:id', (req, res) => {
  const edit_postID = req.params.id

  con.query(
    'SELECT * FROM authors WHERE id=?',
    [edit_postID],
    (err, results) => {
      if (results) {
        res.render('edit', {
          authors: results[0]
        })
      }
    }
  )
})

app.post('/edit/:id', (req, res) => {
  const update_name = req.body.name
  const update_city = req.body.city
  const userId = req.params.id
  con.query(
    'UPDATE `authors` SET name = ?, city = ? WHERE id = ?',
    [update_name, update_city, userId],
    (err, results) => {
      if (err) throw err // หากพบ err  ให้แสดง err
      console.log(`Changed ${results.changedRows} row(s)`)
    }
  )
  return res.redirect('/home')
})

app.get('/delete/:id', (req, res) => {
  con.query(
    'DELETE FROM `authors` WHERE id = ?',
    [req.params.id],
    (err, results) => {
      if (err) throw err // หากพบ err  ให้แสดง err
      console.log(`Deleted ${results.affectedRows} row(s)`) // แสดงผลการลบ หากสำเร็จ
    }
  )
  return res.redirect('/home')
})

/*con.query('SELECT * FROM authors', (err, rows) => {
  if (err) throw err // หากพบ err ให้แสดง err

  console.log('Data received from Db:')
  for (id in rows) {
    console.log(rows[id])
  }
  //console.log(rows) // แสดงข้อมูลเป็น ojb
})*/

/*con.query('SELECT * FROM authors', (err, rows) => {
  if (err) throw err // หากพบ err ให้แสดง err

  console.log('Show in Row from Db :')
  Object.keys(rows).forEach(function (key) {
    // วนซ้ำแถวทั้งหมด เพื่อหา rows และแสดงผล
    var row = rows[key]
    console.log(`${row.name} lives in ${row.city}`) // แสดงข้อมูลชื่อ,เมือง ในแบบที่ไม่เป็น obj
  })
})

/*rows.forEach(row => {
  // เลือกดูเป็นบ้างส่วน
  console.log(`${row.name} lives in ${row.city}`) // แสดงข้อมูลชื่อ,เมือง
})*/

/*const author = { name: 'A', city: 'A' } // ข้อมูลใหม่ อยู่ในรูปแบบ json
con.query('INSERT INTO authors SET ?', author, (err, res) => {
// เป็นคำสั่งเพิ่ม มีการระบุชื่อตาราง
if (err) throw err // หากพบ err  ให้แสดง err

console.log('Last insert ID:', res.insertId) // แสดงผลการเพิ่มเป็นไอดีตัวที่เพิ่มล่าสุด
})


con.query(
  'UPDATE authors SET city = ? Where NAME = ?',
  ['Khonkan', 'Michaela Lehr'],
  (err, result) => {
    // การแก้ไข มีการระบุไอดีของตัวที่ต้องการแก้ไข
    if (err) throw err
    console.log(`Changed ${result.changedRows} row(s)`) // แสดงหากแก้สำเร็จ
  })


con.query('DELETE FROM authors WHERE id = ?', [6], (err, result) => {
  // การลบ ต้องระบุไอดีที่ต้องการลบ
  if (err) throw err
  console.log(`Deleted ${result.affectedRows} row(s)`) // แสดงผลการลบ หากสำเร็จ
})

/*con.query('SELECT * FROM authors WHERE city =?', 'Leipzig', (err, result) => {
  // การแสดงข้อมูลตามเงื่อนไข มีการระบุไอดี
  if (err) throw err
  result.forEach(function (rows) {
    console.log(`Show Result :` + rows)
  })
})*/

/*con.query(
  'SELECT * FROM authors Where ID = ?', 1, (err, result) => {
    if (err) throw err;
    console.log(result);
  }
);*/

app.listen(3000, function () {
  console.log('Server Listen at http://localhost:3000')
  //console.log("Users :", data);
})
