const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const alert = require('alert')
const date = require('date-and-time')
// app.engine('html', require('ejs').renderFile)
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.static(__dirname))
app.use('/images', express.static(__dirname + '/img'))

const mysql = require('mysql')
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'dcoffee'
})

const session = require('express-session')
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
)

con.connect(err => {
  //เช็คว่ามัน connect มั้ย
  if (err) {
    console.log('Error connecting to Db')
    console.log(err)
    return
  }
  console.log('Connection established')
})

/* con.query('SELECT * FROM mst_security', (err, rows) => {
  if (err) throw err
  console.log('Data received from Db:')
  console.log(rows)
})

con.query('SELECT * FROM mst_employee', (err, rows) => {
  if (err) throw err

  console.log('Data received from Db:')
  console.log(rows)
}) */

app.get('/', function (req, res) {
  //แสดง Form login เมื่อค่าจาก url = /
  res.sendFile(__dirname + '/html/' + 'Login.html') //นำข้อมูลไฟล์จากโฟลเดอร์ public ที่ชื่อ Login.html มาแสดง
})

app.post('/user', function (req, res, next) {
  const user = req.body.email //รับค่าที่ส่งมาจาก body จาก tag ที่ชื่อ email
  const password = req.body.password //รับค่าที่ส่งมาจาก body จาก tag ที่ชื่อ password
  if (user && password) {
    con.query(
      'SELECT * FROM mst_security WHERE user=? AND password=?',
      [user, password],
      (err, results) => {
        if (results.length > 0) {
          req.session.loggedin = true
          req.session.user = user
          var id = results[0].id_employee
          const now = new Date()
          const timein = {
            datetime_login: date.format(now, 'YYYY-MM-DD HH:mm:ss'),
            id_employee: id
          }
          con.query('INSERT INTO trn_login SET ?', timein, (err, intime) => {
            if (err) throw err
            var tin = intime.insertId
            console.log('Last insert Timein:', tin)
          })
          con.query(
            'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
            [user],
            (err, result) => {
              if (err) throw err

              if (result[0].position === 'เจ้าของร้าน') {
                res.redirect('/main')
              } else {
                res.redirect('/main_emp')
              }
            }
          )
        } else {
          // res.render('noMember')
          alert('คุณไม่เป็นสมาชิก กรุณาลองอีกครั้ง!')
        }
      }
    )
  } else {
    // res.render('error')
    alert('กรอกข้อมูลไม่ถูกต้อง กรุณลองอีกครั้ง!')
  }
})

app.get('/main', (req, res) => {
  con.query(
    'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
    [req.session.user],
    (err, result) => {
      if (err) throw err
      console.log(result)
      res.render('main', {
        detail: 'ชื่อ: ' + result[0].name + ' ' + result[0].surname
      })
    }
  )
})

app.get('/main_emp', (req, res) => {
  con.query(
    'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
    [req.session.user],
    (err, result) => {
      if (err) throw err
      console.log(result)
      res.render('main_emp', {
        detail: 'ชื่อ: ' + result[0].name + ' ' + result[0].surname
      })
    }
  )
})

app.get('/member', (req, res) => {
  if (req.session.loggedin) {
    con.query(
      'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
      [req.session.user],
      (err, result) => {
        if (err) throw err
        console.log(result)
        var count = 0

        con.query(
          'SELECT datetime_login FROM trn_login WHERE id_employee=?',
          [result[0].id_employee],
          (err, fil) => {
            if (err) throw err
            count = fil.length
            con.query(
              'SELECT datetime_logout FROM trn_logout WHERE id_employee=?',
              [result[0].id_employee],
              (err, fill) => {
                if (err) throw err

                if (fill.length > 0) {
                  res.render('member', {
                    detail: result[0].name + ' ' + result[0].surname,
                    detail2: result[0].id_employee,
                    detail3: result[0].position,
                    detail4: result[0].salary.toFixed(2),
                    detail5: result[0].total_sale.toFixed(2),
                    detail6: fil[0].datetime_login,
                    detail7: fill[0].datetime_logout,
                    detail8: count
                  })
                } else {
                  res.render('member', {
                    detail: result[0].name + ' ' + result[0].surname,
                    detail2: result[0].id_employee,
                    detail3: result[0].position,
                    detail4: result[0].salary.toFixed(2),
                    detail5: result[0].total_sale.toFixed(2),
                    detail6: fil[0].datetime_login,
                    detail7: ' ',
                    detail8: count
                  })
                }
              }
            )
          }
        )
      }
    )
  }
})

app.get('/search', function (req, res) {
  con.query(
    'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
    [req.session.user],
    (err, result) => {
      if (err) throw err

      con.query('SELECT * FROM mst_employee', (err, rows) => {
        if (err) throw err

        res.render('search', { mst_employee: rows })
      })
    }
  )
})

app.get('/search/:emp', function (req, res) {
  con.query(
    'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
    [req.session.user],
    (err, result) => {
      if (err) throw err
      let { emp } = req.params

      con.query(
        'SELECT * FROM mst_employee WHERE name LIKE "%' +
          emp +
          '%" OR surname LIKE "%' +
          emp +
          '%" OR position LIKE "%' +
          emp +
          '%" OR salary LIKE "%' +
          emp +
          '%" OR total_sale LIKE "%' +
          emp +
          '%" OR id_employee LIKE "%' +
          emp +
          '%"',
        function (err, rows, fields) {
          if (err) throw err
          for (i = 0; i < rows.length; i++) {
            console.log('Search Success')
          }
          res.render('search', { mst_employee: rows })
        }
      )
    }
  )
})

app.get('/register', function (req, res) {
  res.render('register')
})

app.post('/register_emp', function (req, res) {
  const name = req.body.name
  const surname = req.body.surname
  const position = req.body.position
  const salary = req.body.salary
  const total_sale = req.body.total_sale
  const user = req.body.user
  const password = req.body.password
  const author = {
    name: name,
    surname: surname,
    position: position,
    salary: salary,
    total_sale: total_sale
  }
  con.query('INSERT INTO mst_employee SET ?', author, (err, int) => {
    if (err) throw err // หากพบ err  ให้แสดง err
    console.log('Last insert ID employee : ', int.insertId)
    const author2 = {
      user: user,
      password: password,
      id_employee: int.insertId
    }
    con.query('INSERT INTO mst_security SET ?', author2, (err, se) => {
      if (err) throw err // หากพบ err  ให้แสดง err
      console.log('Last insert ID security : ', se.insertId)
      // res.status(201)
    })
    con.query(
      'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
      [req.session.user],
      (err, result) => {
        if (err) throw err
        // console.log(result)
        return res.redirect('/main')
      }
    )
  })
})

// SELECT mst_employee.id_employee, name, surname, position, salary, total_sale,user,password FROM mst_employee INNER JOIN  mst_security ON mst_employee.id_employee = mst_security.id_employee WHERE mst_employee.id_employee=00001
/* app.get('/edit', function (req, res) {
  res.render('edit')
}) */

app.get('/edit/:id', function (req, res) {
  var { id } = req.params
  console.log(id)
  con.query(
    'SELECT mst_employee.id_employee, name, surname, position, salary, total_sale,user,password FROM mst_employee INNER JOIN  mst_security ON mst_employee.id_employee = mst_security.id_employee WHERE mst_employee.id_employee=?',
    [id],
    (err, result) => {
      if (err) throw err
      console.log(result)
      res.render('edit', {
        detail: result[0].name,
        detail1: result[0].surname,
        detail2: result[0].id_employee,
        detail3: result[0].position,
        detail4: result[0].salary.toFixed(2),
        detail5: result[0].total_sale.toFixed(2),
        detail6: result[0].user,
        detail7: result[0].password
      })
    }
  )
  /* con.query(
    'SELECT * FROM mst_security JOIN mst_employee ON mst_employee.id_employee=mst_security.id_security WHERE user=?',
    [req.session.user],
    (err, results) => {
      if (err) throw err
      // console.log(result)
      return res.redirect('/main')
    }
  ) */
})

app.post('/edit_emp',function(req,res){
  const id = req.body.id
  console.log(id)
  var emp = {
    name: req.body.name,
    surname: req.body.surname,
    position: req.body.position,
    salary: req.body.salary,
    total_sale: req.body.total_sale
  }
  con.query('UPDATE mst_employee SET ? WHERE id_employee = ?',[emp,id],(err,result)=>{
    if (err) throw err
    var  sec = {
      user: req.body.user,
      password: req.body.password
    }
    console.log(`Changed emp : ${result.changedRows} row(s)`)

    con.query('UPDATE mst_security SET ? WHERE id_employee = ?',[sec,id],(err,results)=>{
      if (err) throw err
      console.log(`Changed sec : ${results.changedRows} row(s)`)
    })
  })
  return res.redirect('/main')
})

/* app.get('/del', function (req, res) {
  res.render('del')
}) */

app.get('/del/:id', function (req, res) {
  var { id } = req.params
  con.query(
    'SELECT mst_employee.id_employee, name, surname, position, salary, total_sale,user,password FROM mst_employee INNER JOIN  mst_security ON mst_employee.id_employee = mst_security.id_employee WHERE mst_employee.id_employee=?',
    [id],
    (err, result) => {
      if (err) throw err
      res.render('del', {
        detail: result[0].name,
        detail1: result[0].surname,
        detail3: result[0].position,
        detail2: result[0].id_employee
      })
    }
  )
})

app.get('/del_emp/:id', function (req, res){
  var { id } = req.params
  con.query('DELETE FROM trn_logout WHERE id_employee = ?',[id],(err,result)=>{
    if (err) throw err
    console.log(`Del Timeout : ${result.affectedRows} row(s)`)
  })
  con.query('DELETE FROM trn_login WHERE id_employee = ?',[id],(err,result)=>{
    if (err) throw err
    console.log(`Del Timein : ${result.affectedRows} row(s)`)
  })
  con.query('DELETE FROM mst_security WHERE id_employee = ?',[id],(err,result)=>{
    if (err) throw err
    console.log(`Del user : ${result.affectedRows} row(s)`)
  })
  con.query('DELETE FROM mst_employee WHERE id_employee = ?',[id],(err,result)=>{
    if (err) throw err
    console.log(`Del Data : ${result.affectedRows} row(s)`)
  })
  res.redirect('/main')
})

app.get('/out', function (req, res) {
  con.query(
    'SELECT * FROM mst_security WHERE user=?',
    [req.session.user],
    (err, result) => {
      var id = result[0].id_employee
      const now = new Date()
      const timeout = {
        datetime_logout: date.format(now, 'YYYY-MM-DD HH:mm:ss'),
        id_employee: id
      }
      con.query('INSERT INTO trn_logout SET ?', timeout, (err, intime) => {
        if (err) throw err
        console.log('Last insert Timeout:', intime.insertId)
        return res.redirect('/')
      })
    }
  )
})

app.listen(3000, function () {
  console.log('Server Listen at http://localhost:3000')
})
